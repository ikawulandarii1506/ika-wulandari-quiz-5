<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>67bcd032-0871-4a1b-a825-516f9268d8f2</testSuiteGuid>
   <testCaseLink>
      <guid>6ec5d4b7-5b99-4703-b5da-a42dc92f522a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC Manual - DDT</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b286adc2-c6d0-4cd7-b29f-3b376c1a6a33</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Data User</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>b286adc2-c6d0-4cd7-b29f-3b376c1a6a33</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>e60b6d5b-3935-4026-88ab-51bfa4db663a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b286adc2-c6d0-4cd7-b29f-3b376c1a6a33</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>9cc086fa-7f93-4b25-8c74-543bf7e51a10</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
