import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://katalon-demo-cura.herokuapp.com/')

WebUI.click(findTestObject('HomePage/Page_CURA Healthcare Service/btn_menu-toogle'))

WebUI.click(findTestObject('HomePage/Page_CURA Healthcare Service/btn_menu_Login'))

WebUI.setText(findTestObject('Login/Page_CURA Healthcare Service/txtField_username'), 'John Doe')

WebUI.setText(findTestObject('Login/Page_CURA Healthcare Service/txtField_password'), 'ThisIsNotAPassword')

WebUI.click(findTestObject('Login/Page_CURA Healthcare Service/btn_Login'))

WebUI.waitForElementPresent(findTestObject('Login/Page_CURA Healthcare Service/drop_facility'), 3)

WebUI.selectOptionByValue(findTestObject('Make Appointment/Page_CURA Healthcare Service/select_facity'), 'Tokyo CURA Healthcare Center', 
    false)

WebUI.click(findTestObject('Make Appointment/Page_CURA Healthcare Service/input_Apply'))

WebUI.click(findTestObject('Make Appointment/Page_CURA Healthcare Service/lbl_Medicaid'))

WebUI.click(findTestObject('Make Appointment/Page_CURA Healthcare Service/lbl_Medicare'))

WebUI.click(findTestObject('Make Appointment/Page_CURA Healthcare Service/lbl_None'))

WebUI.click(findTestObject('Make Appointment/Page_CURA Healthcare Service/date'))

WebUI.click(findTestObject('Appointment_confirmation/Page_CURA Healthcare Service/td_30'))

WebUI.setText(findTestObject('Make Appointment/Page_CURA Healthcare Service/txt_comment'), 'aaa')

WebUI.click(findTestObject('Make Appointment/Page_CURA Healthcare Service/btn_Book Appointment'))

WebUI.waitForElementPresent(findTestObject('Appointment_confirmation/Page_CURA Healthcare Service/btn_Go to Homepage'), 
    3)

WebUI.closeBrowser()

